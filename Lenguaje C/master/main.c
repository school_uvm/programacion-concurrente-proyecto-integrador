#include <stdio.h>
#include <stdlib.h>
#include <pvm3.h>

#include "../utilities/operaciones.h"

/* -----------------------------------------------------
 * Constantes
 * ----------------------------------------------------- */

#define SLAVE_NAME "slave"
#define EMPTY_ARGV (char**) 0

#define REMOTE_ID ""
#define DEFAULT_CONTENT 2.0f

/* -----------------------------------------------------
 * Estructuras
 * ----------------------------------------------------- */

/**
 * Estructura que guarda la información del programa
 */
struct PvmCInfo {
    /**
     * El id de los procesos hijos
     */
    int cIds[20];

    /**
     * Cantidad de datos que se enviarán a los hijos
     */
    int nData;

    /**
     * Cantidad de procesos creados
     */
    int nProc;

    /**
     * Cantidad de procesos creados satisfactoriamente.
     * <p>
     * Si el proceso es un numero negativo, entonces se trata de un error.
     */
    int nvProc;

    /**
     * El numero del proceso que termina con su trabajo
     */
    int who;

    /**
     * Tipo de mensaje esperado
     */
    int msgType;

    /**
     * Cantidad de comunicaciones entre padre e hijo
     */
    int loops;

    /**
     * Información que se enviará
     */
    float data[10];
};

/* -----------------------------------------------------
 * Funciones
 * ----------------------------------------------------- */

/**
 * Se inicializa la información que se enviará a los procesos hijos
 *
 * @param info información del programa
 * @param content el contenido que se rellenará
 */
void inicializarData(struct PvmCInfo *info, float content) {
    // Iniciamos el tamaño de los datos
    info->nData = 10;
    // Llenamos la información con el contenido
    for (int i = 0; i < info->nData; ++i) {
        info->data[i] = content;
    }
}

/**
 * Iniciamos los elementos base del proceso padre
 *
 * @param info la información del programa
 */
void iniciarPadre(struct PvmCInfo *info) {
    // Variable para guardar el error
    int error;

    // Preguntamos por los hijos
    printf("Cuantos hijos (1-120)? ");
    info->nProc = askIntD(1, 120, &error);
    if (error != 0) exit(error);

    // Preguntamos por las comunicaciones
    printf("Cuantos bucles de comunicación (1 - 5000)? ");
    info->loops = askIntD(1, 5000, &error);
    if (error != 0) exit(error);
}

/**
 * Configuramos todos los elementos hijos del programa
 *
 * @param info la información del programa
 */
void configurarHijos(struct PvmCInfo *info) {
    // Redireccionamos la salida de los
    // procesos hijos
    pvm_catchout(stdout);

    // Creamos a los procesos hijos
    // 1. Nombre del proceso
    // 2. Argumentos del programa
    // 3. Opción que controla el comportamiento del proceso
    // 4. Ubicación de donde se ejecutara el proceso
    // 5. El número de instancias que se crearán
    // 6. Un array para guardar los identificadores de los procesos
    // Esta función devuelve el número de procesos creados o un número
    // negativo si ocurrió un error al crearlos
    info->nvProc = pvm_spawn(
            SLAVE_NAME,
            EMPTY_ARGV,
            PvmTaskDefault,
            REMOTE_ID,
            info->nProc,
            info->cIds);
    // Mostramos información del proceso de creación
    printf("Resultado de creación: %d\n", info->nvProc);

    // Verificamos si hubo algún error
    if (info->nvProc < info->nProc) {
        // Mostramos información del error
        printf("Error al crear procesos hijos\n");
        for (int i = info->nvProc; i < info->nProc; i++) {
            // Mostramos la información
            printf("Proceso (%d) con id %d\n", i, info->cIds[i]);
        }

        // Matamos los procesos debido a que hubo un error en su creación
        for (int i = 0; i < info->nProc; i++) {
            pvm_kill(info->cIds[i]);
        }

        // Finalizamos el programa
        pvm_exit();
        exit(1);
    }

    // Iniciamos la información base que se enviará
    inicializarData(info, DEFAULT_CONTENT);

    // Limpiamos el buffer y preparamos para el empaquetamiento
    // de información válida que se quiere enviar
    pvm_initsend(PvmDataDefault);

    // Empaquetamos el contenido a enviar
    pvm_pkint(&info->loops, 1, 1);
    pvm_pkint(&info->nProc, 1, 1);
    pvm_pkint(info->cIds, info->nProc, 1);
    pvm_pkint(&info->nData, 1, 1);

    pvm_pkfloat(info->data, info->nData, 1);
    pvm_mcast(info->cIds, info->nProc, 0);

    // Esperamos el resultado de alguno de los hijos
    info->msgType = 5;

    // Iteramos todos los procesos hijos
    for (int i = 0; i < info->nProc; i++) {
        // Espera a recibir un mensaje de cualquiera de los hijos
        // el `msType` es un tag que identifica el mensaje esperado
        pvm_recv(-1, info->msgType);

        // Desempaquetamos la información de quien fue el hijo que respondió
        pvm_upkint(&info->who, 1, 1);

        // Mostramos la información del proceso que terminó
        printf("El proceso con id (%d) terminó\n", info->who);
    }

    // Cerramos el proceso padre
    pvm_exit();
}

/**
 * Función principal del programa
 *
 * @return resultado final del programa
 */
int main() {
    // Creamos la información para usarla como referencia
    struct PvmCInfo info;

    // Iniciamos las propiedades dependiendo del
    // estado del padre
    if (pvm_parent() == PvmNoParent) {
        iniciarPadre(&info);
    }

    // Configuramos los procesos hijos
    configurarHijos(&info);

    return 0;
}