# **************************************************
# * Functions
# **************************************************

function(resolve_definitions PREFIX FILES OUTPUT)
    # Temporal variables
    set(TMP_RESULT)

    string(LENGTH ${PREFIX} P_LENGTH)
    math(EXPR INDEX "${P_LENGTH} - 1")
    string(SUBSTRING ${PREFIX} ${INDEX} 1 LAST_C)

    # Validate prefix slash
    if (NOT LAST_C STREQUAL "/" OR NOT LAST_C STREQUAL "\\")
        string(APPEND PREFIX /)
    endif ()

    # Generate components
    foreach (IT IN LISTS ${FILES})
        set(TMP_IT ${PREFIX}${IT})
        string(APPEND TMP_RESULT ${TMP_IT} ";")
    endforeach ()

    set(${OUTPUT} ${TMP_RESULT} PARENT_SCOPE)
endfunction()

function(dirname LOCATION OUTPUT)
    set(TMP_OUT)
    get_filename_component(TMP_OUT ${LOCATION} DIRECTORY)
    set(${OUTPUT} ${TMP_OUT} PARENT_SCOPE)
endfunction()