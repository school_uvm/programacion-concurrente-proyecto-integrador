#include <stdio.h>
#include <stdlib.h>
#include <pvm3.h>

#include "../utilities/operaciones.h"

/* -----------------------------------------------------
 * Constantes
 * ----------------------------------------------------- */

#define SLAVE_DATA_SEND 22

/* -----------------------------------------------------
 * Estructuras
 * ----------------------------------------------------- */

/**
 * Información del proceso hijo actual
 */
struct SlaveInfo {

    /**
     * Id del proceso hijo
     */
    int id;

    /**
     * Numero de indice dentro del montón de procesos
     */
    int index;

    /**
     * El id de los procesos hijos
     */
    int cIds[20];

    /**
     * Cantidad de datos que se enviarán a los hijos
     */
    int nData;

    /**
     * Cantidad de procesos creados
     */
    int nProc;

    /**
     * Tipo de mensaje esperado
     */
    int msgType;

    /**
     * Cantidad de comunicaciones entre padre e hijo
     */
    int loops;

    /**
     * Información que se enviará
     */
    float data[10];
};

/**
 * Estructura que guarda información temporal del trabajo
 * de los procesos hijos
 */
struct SlaveTeamInfo {

    /**
     * El indice que se calcula con cada iteración
     */
    int index;

    /**
     * El destinatario que se encargará de realizar el trabajo
     */
    int dest;

    /**
     * Suma parcial
     */
    float pSum;

    /**
     * Cantidad de información que se debe agregar en cada
     * iteración
     */
    float aSum;
};

/* -----------------------------------------------------
 * Funciones
 * ----------------------------------------------------- */

/**
 * Inicializamos la información para que el programa funcione correctamente
 * @param info información del programa
 */
struct SlaveInfo inicializarInfo() {
    // Creamos la información del proceso
    struct SlaveInfo result;

    // Inicializamos los datos más importantes
    result.msgType = 0;
    result.id = pvm_mytid();

    // Obtenemos los datos del padre
    pvm_recv(-1, result.msgType);

    pvm_upkint(&result.loops, 1, 1);
    pvm_upkint(&result.nProc, 1, 1);
    pvm_upkint(result.cIds, result.nProc, 1);
    pvm_upkint(&result.nData, 1, 1);
    pvm_upkfloat(result.data, result.nData, 1);

    return result;
}

/**
 * Envía la información a los otros procesos vecinos
 * @param info la información que se enviará
 * @return devuelve el trabajo en equipo
 */
struct SlaveTeamInfo trabajoEnEquipo(struct SlaveInfo *info) {
    // Creamos la información del trabajo
    struct SlaveTeamInfo teamWork = {.pSum= 0.0f, .aSum = 0.1f};

    // Iteramos las veces que según la información
    // del proceso padre
    for (int i = 1; i <= info->loops; i++) {
        // Iniciamos el empaquetamiento de datos
        pvm_initsend(PvmDataDefault);

        pvm_pkfloat(&teamWork.aSum, 1, 1);

        // Definimos el siguiente destino
        // y enviamos información
        teamWork.dest = limitOverflowArr(info->id + 1, 0, info->nProc);
        pvm_send(info->cIds[teamWork.dest], SLAVE_DATA_SEND);

        // Definimos el índice que nos enviará información
        // y leemos la información
        teamWork.index = limitOverflowArr(info->id - 1, 0, info->nProc);
        pvm_recv(info->cIds[teamWork.index], SLAVE_DATA_SEND);
        pvm_upkfloat(&teamWork.pSum, 1, 1);
    }

    return teamWork;
}

/**
 * Función principal del programa
 *
 * @return estado final del programa
 */
int main() {
    // Creamos la información del proceso
    struct SlaveInfo info = inicializarInfo();

    // Buscamos el índice del proceso actual
    for (int i = 0; i < info.nProc; i++) {
        if (info.id == info.cIds[i]) {
            // Asignamos el índice de la información
            info.index = i;
            break;
        }
    }

    // Realizamos lo mismo para los procesos vecinos
    struct SlaveTeamInfo teamInfo = trabajoEnEquipo(&info);

    // Enviamos al padre la información
    pvm_initsend(PvmDataDefault);
    pvm_pkint(&info.id, 1, 1);

    // Definimos el tipo de mensaje
    // que es el que espera el proceso padre
    info.msgType = 5;

    // Obtenemos el proceso padre
    int master = pvm_parent();
    pvm_send(master, info.msgType);

    pvm_exit();
    return 0;
}