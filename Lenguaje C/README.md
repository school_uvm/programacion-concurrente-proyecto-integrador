# Proyecto Integrador Equipo 8

- Materia: Programación concurrente
- Universidad: Universidad Valle de México
- Participantes:
    - Alvarez Ayala Brian Eduardo
    - Herrera Daniela
    - Medrano Mendez Jaqueline


# Herramientas utilizadas

- CMake: Utilizada para compilar el codigo de manera automatica
    - Version: 3.20
- Ninja: Sistema de compilacion continua
    - Version: 1.10.0-1build1
- Compiladores compatibles:
    - GCC: 9.4.0
- PVM3
    - Version: 3.4.6-2build2

# Distribuciones Linux probadas

- Ubuntu
    - Nativo: 22.04.1 LTS amd64
    - WSL: 20.04.5 LTS on Windows 10 x86_64


# Como compilar

Antes de compilar primero instalamos todas las dependencias en la maquina. Si se esta usando Ubuntu o cualquier distribucion Debian debe ejecutar el siguiente comando:

```bash
Ubuntu:~/$ sudo apt install gcc g++ pvm pvm-dev cmake ninja-build openssh-server
```

Para cualquier otra distribucion linux debe buscar e instalar los siguientes paquetes:

- gcc (Compilador C)
- g++ (Compilador C++)
- pvm (Pararel Virtual Machine)
- pvm-dev (Paralel Virtual Machine - Developer tools)
- cmake (Sistema de compilación)
- ninja-build (Sistema de configuracion de compilación)
- openssh-server (Herramientas de coomunicación SSH)

Despues debe de crearse la configuración, para que sea posible compilar el Proyecto.
Es por eso que primero debe de ejecutar el siguiente comando:

```bash
Ubuntu:~/$ cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/gcc -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++ "-S/carpeta_del_codigo_fuente" "-B/carpeta_del_codigo_fuente/build" -G Ninja
```

Este comando solo se ejecuta una vez y se tienen que remplazar el algunas partes como:

- Primero ```-S/carpeta_del_codigo_fuente``` se tiene que remplazar por la carpeta donde se encentra el proyecto
    - Por ejemplo si la carpeta de tu proyecto es ```/home/-user-/Escritorio/MyProyecto``` entonces queraría de la siguiente manera: ```-S/home/-user-/Escritorio/MyProyecto```
    - Notese que tanto como las comillas y ```-S``` deben estar presentes
- Segundo ```-S/carpeta_del_codigo_fuente/build``` se tiene que remplazar por la carpeta donde se encuentra el proyecto y opcionalmente se puede cambiar el nombre de ```build``` por el nombre que desee.
    - la carpeta ```build``` es utilizada para guardar la configuracion de CMake y aquí es donde se generaran los ejecutables del proyecto

Una vez ejecutado el comando, es posible compilar el proyecto:

```bash
Ubuntu:carpetaDelProyecto/$ cmake --build ./build --config Debug --target __configuracion__
```

Este es el comando que se debe ejecutar. Pero en el apartado de ```--target __configuracion__``` se debe de remplazar **__configuracion__** por el nombre del proyecto que se quiere compilar. En todo el proyecto existen 3 subproyectos llamados:

- master
- slave
- ejercicio_1

si se quiere compilar el ejercicio ```master``` debe hacerce de la siguiente manera:

```bash
Ubuntu:carpetaDelProyecto/$ cmake --build ./build --config Debug --target master
```

Esto tambien aplica para los demas subproyectos.
