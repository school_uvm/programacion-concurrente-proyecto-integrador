cmake_minimum_required(VERSION 3.20)
project(PC_ProyectoIntegrador1 C)

set(CMAKE_C_STANDARD 11)
include(functions.cmake)

# **************************************************
# * Inclusion de proyectos
# **************************************************

include(${CMAKE_SOURCE_DIR}/utilities/CMakeLists.txt)

include(${CMAKE_SOURCE_DIR}/ejercicio_1/CMakeLists.txt)
include(${CMAKE_SOURCE_DIR}/master/CMakeLists.txt)
include(${CMAKE_SOURCE_DIR}/slave/CMakeLists.txt)