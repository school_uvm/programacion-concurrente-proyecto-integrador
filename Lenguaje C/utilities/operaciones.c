#include "operaciones.h"

/* -----------------------------------------------------
 * Variables globales
 * ----------------------------------------------------- */

/**
 * Buffer utilizado para guardar información
 */
char gBuffer[255];

/**
 * Buffer utilizado para guardar el residuo de los datos
 */
char *gBucket;

/* -----------------------------------------------------
 * Funciones
 * ----------------------------------------------------- */

llong Min(llong a, llong b) {
    return a > b ? b : a;
}

llong Max(llong a, llong b) {
    return a > b ? a : b;
}

int isNull(void *ptr) {
    return ptr == NULL;
}

int isNotNull(void *ptr) {
    return !isNull(ptr);
}

int askInt(int min, int max, int *error, int base) {
    // Se reinicia el error para prevenir confusion
    // con otro tipo de errores
    errno = 0;
    if (isNotNull(error)) {
        *error = 0;
    }
    // Preguntamos por la información
    int c = scanf("%s", gBuffer);
    if (c < 0) {
        printf("Error to get some value");
        if (isNotNull(error)) {
            *error = -1;
        }
    }

    // Convertimos el contenido a un número válido
    long result = strtol(gBuffer, &gBucket, base);
    if (gBucket == gBuffer) {
        printf("El contenido no es un numero valido\n");
        if (isNotNull(error)) {
            *error = -2;
        }
    } else if (errno != 0 && result == 0) {
        printf("Error al convertir el contenido\n");
        if (isNotNull(error)) {
            *error = errno;
        }
    }

    result = Max(min, result);
    result = Min(max, result);
    return (int) result;
}

int askIntD(int min, int max, int *error) {
    return askInt(min, max, error, 10);
}

int limitOverflow(int current, int min, int max) {
    // Verificamos que el valor este dentro del rango
    if (current > max) return min;
    if (current < min) return max;

    return current;
}

int limitOverflowArr(int current, int min, int max) {
    // Verificamos que el valor este dentro del rango
    if (current > max) return min;
    if (current < min) return max - 1;

    return current;
}