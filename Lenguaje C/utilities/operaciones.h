#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#ifndef SLAVE_OPERACIONES_H
#define SLAVE_OPERACIONES_H

/* -----------------------------------------------------
 * Definiciones
 * ----------------------------------------------------- */

/**
 * Alias del tipo long long
 */
typedef long long llong;

/* -----------------------------------------------------
 * Funciones
 * ----------------------------------------------------- */

/**
 * Devuelve el elemento mínimo entre los dos números pasados
 * @param a primer numero
 * @param b segundo numero
 * @return el numero más pequeño
 */
llong Min(llong a, llong b);

/**
 * Devuelve el elemento máximo entre los dos números pasados
 * @param a primer numero
 * @param b segundo numero
 * @return el numero más grande
 */
llong Max(llong a, llong b);

/**
 * Verifica que el puntero seleccionado sea un tipo nulo
 * @param ptr el puntero de datos
 * @return devuelve 0 si el puntero es valido o 1 si el puntero es nulo
 */
int isNull(void *ptr);

/**
 * Verifica que el puntero seleccionado no sea un tipo nulo
 * @param ptr el puntero de datos
 * @return devuelve 0 si el puntero es nulo o 1 si el puntero es valido
 */
int isNotNull(void *ptr);

/**
 * Realiza una pregunta por consola de la información requerida
 * @param min valor mínimo
 * @param max valor maximo
 * @param error error al convertir el contenido
 * @param base base del numero a convertir
 * @return devuelve el numero insertado por consola
 */
int askInt(int min, int max, int *error, int base);

/**
 * Realiza una pregunta por consola de la información requerida.
 * <p>
 * La base por defecto es "10", para números decimales
 * @param min valor mínimo
 * @param max valor maximo
 * @param error error al convertir el contenido
 * @return devuelve el numero insertado por consola
 */
int askIntD(int min, int max, int *error);

/**
 * Limita un contenido dentro de un rango.
 * Si este es mayor que el rango especificado, se reinicia asi para las dos direcciones.
 *
 * @param current el numero que se quiere limitar
 * @param min el valor mínimo
 * @param max el valor máximo
 * @return un valor valido dentro de los dos rangos
 */
int limitOverflow(int current, int min, int max);

/**
 * Limita un contenido dentro de un rango.
 * Si este es mayor que el rango especificado, se reinicia asi para las dos direcciones.
 * <p>
 * Esta función se utiliza para arrays y el maximo de valores se le resta una unidad por defecto.
 *
 * @param current el numero que se quiere limitar
 * @param min el valor mínimo
 * @param max el valor máximo
 * @return un valor valido dentro de los dos rangos
 */
int limitOverflowArr(int current, int min, int max);

#endif //SLAVE_OPERACIONES_H