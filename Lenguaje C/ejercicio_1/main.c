#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**
 * Impresión de las instrucciones del programa
 */
void imprimirInstrucciones() {
    printf("---------------------------------------------------------\n");
    printf("Para utilizar el programa es necesario: \n");
    printf("Uso de un numero inicial \n");
    printf("Uso de un numero final \n");
    printf("---------------------------------------------------------\n");
}

/**
 * Función principal del programa
 * @param argc el total de argumentos del programa
 * @param argv los argumentos del programa
 * @return el estado final del programa
 */
int main(int argc, char **argv) {
    // Verificamos que el número de argumentos sea al menos 2
    if (argc < 2) {
        imprimirInstrucciones();
        return 1;
    }

    // Variables utilizadas para guardar resultados
    float n1 = strtof(argv[1], NULL),
            n2 = strtof(argv[2], NULL),
            tmp, rs = 0.0f;

    // Iterar los elementos del problema
    for (tmp = n1; tmp <= n2; tmp++) {
        rs += tmp;
    }

    printf("Resultado: %f\n", rs);
    return 0;
}