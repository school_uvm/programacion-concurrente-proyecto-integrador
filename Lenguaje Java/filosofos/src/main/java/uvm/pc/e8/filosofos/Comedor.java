package uvm.pc.e8.filosofos;

import uvm.pc.e8.utilidades.EntradaUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Mesa principal donde se encuentran los filósofos.
 * La mesa es la encargada de administrar los tenedores (o palillos dependiendo del problema)
 * y previene una condición de carrera o algún interbloqueo.
 */
public class Comedor {
	
	/**
	 * Mensaje utilizado cuando los filósofos terminan de comer
	 */
	private static final String MSG_FILOSOFO_TERMINA = "%s Deja de comer y los tenedores [%s y %s] ahora están libres";
	
	/* ---------------------------------------------------------
	 * Propiedades
	 * --------------------------------------------------------- */
	
	/**
	 * Instancia de registro para esta clase
	 */
	private static final Logger LOG = Logger.getLogger(Comedor.class.getName());
	
	/**
	 * Objeto utilizado para sincronizar los hilos y prevenir
	 * condiciones de carrera
	 */
	private final Object candado = new Object();
	
	/**
	 * Lista con todos los palillos de la mesa
	 */
	private final List<Tenedor> tenedores = new ArrayList<>();
	
	/**
	 * Lista con todos los filósofos que comerán en la mesa
	 */
	private final List<Filosofo> filosofos = new ArrayList<>();
	
	/* ---------------------------------------------------------
	 * Constructores
	 * --------------------------------------------------------- */
	
	/**
	 * Constructor por defecto
	 *
	 * @param totalPersonas el total de filósofos que se van a sentar
	 *                      a comer
	 */
	public Comedor(int totalPersonas) {
		// La cantidad de personas no debe ser menor o igual a 0
		// esto no causaría ningún error, pero tampoco sería posible
		// observar el comportamiento del programa
		if (totalPersonas <= 0) {
			throw new IllegalArgumentException("El total de personas no debe ser <= 0");
		}
		
		// Creamos los tenedores que se van a usar
		// También creamos los filósofos
		for (int i = 0; i < totalPersonas; i++) {
			filosofos.add(new Filosofo(this, i));
			tenedores.add(new Tenedor(i));
		}
	}
	
	/* ---------------------------------------------------------
	 * Métodos
	 * --------------------------------------------------------- */
	
	/**
	 * Método utilizado por los filósofos, donde estos intentan agarrar los
	 * tenedores y se ponen a comer. Si algún tenedor contiguo no está disponible,
	 * entonces el filósofo se pone a pensar y espera su turno para comer.
	 *
	 * @param indicePersona la posición del filósofo en la mesa
	 */
	public void intentarAgarrarTenedores(int indicePersona) {
		// Debido a que este método es accesible por otros objetos que no son filósofos
		// es posible que se ingrese un valor fuera del rango de datos
		if (indicePersona < 0 || indicePersona > filosofos.size()) {
			throw new IllegalArgumentException("El indice está fuera del rango permitido");
		}
		
		// Sincronizamos los hilos con el candado de la clase.
		// De esta manera es posible prevenir interbloqueos o
		// condiciones de carrera, dando como resultado el comportamiento
		// que se tiene previsto
		synchronized (candado) {
			// Obtenemos los tenedores contiguos del filosofo
			Tenedor derecho = obtenerTenedorDerecho(indicePersona),
				izquierdo = obtenerTenedorIzquierda(indicePersona);
			
			// Verificamos que los tenedores estén libres.
			// Si no lo están, entonces esperamos a que alguno de los
			// filósofos termine de comer y así poder agarrar el tenedor
			while (derecho.estaOcupado() || izquierdo.estaOcupado()) {
				// Hacemos esperar al objeto candado para no así poder actualizar
				// los estados sin tener que realizar pasos extras
				try {
					candado.wait();
				} catch (InterruptedException e) {
					LOG.log(Level.SEVERE, null, e);
				}
			}
			
			// Una vez terminada la espera, ahora es posible
			// agarrar los tenedores y que el filósofo pueda comer
			derecho.cambiaOcupado(true);
			izquierdo.cambiaOcupado(true);
		}
	}
	
	/**
	 * Método utilizado por los filósofos, donde estos dejan los tenedores
	 * que ocuparon para comer. Dando la oportunidad a los otros filósofos
	 * a poder comer.
	 *
	 * @param indicePersona la posición del filósofo en la mesa
	 */
	public void intentarDejarTenedores(int indicePersona) {
		// Debido a que este método es accesible por otros objetos que no son filósofos
		// es posible que se ingrese un valor fuera del rango de datos
		if (indicePersona < 0 || indicePersona > filosofos.size()) {
			throw new IllegalArgumentException("El indice está fuera del rango permitido");
		}
		
		// Sincronizamos los hilos con el candado de la clase.
		// De esta manera es posible prevenir interbloqueos o
		// condiciones de carrera, dando como resultado el comportamiento
		// que se tiene previsto
		synchronized (candado) {
			// Obtenemos los tenedores contiguos del filosofo
			Tenedor derecho = obtenerTenedorDerecho(indicePersona),
				izquierdo = obtenerTenedorIzquierda(indicePersona);
			
			// Cambiamos el estado de los tenedores, para que los demás
			// filósofos puedan obtenerlos.
			derecho.cambiaOcupado(false);
			izquierdo.cambiaOcupado(false);
			
			// Mostramos un mensaje donde damos a entender que el filósofo
			// ha terminado de comer y los tenedores ahora están libres
			LOG.info(String.format(MSG_FILOSOFO_TERMINA,
								   filosofos.get(indicePersona),
								   derecho,
								   izquierdo));
			
			// Ahora que el filósofo terminó de comer es momento de notificar
			// a los demás objetos que dejen de esperar.
			// Si no realizamos esto, entonces los demás filósofos se quedarán
			// eternamente esperando y morirán de ambre
			candado.notifyAll();
		}
	}
	
	/**
	 * Empieza el proceso de comida en esta mesa.
	 * Este proceso terminará, hasta que todos los filósofos queden satisfechos
	 */
	public void iniciarComida() {
		// Este método lo único que hace es iniciar
		// los hilos (o sea los Filósofos) para que el programa
		// realice el proceso definido dentro del problema
		for (Filosofo filosofo : filosofos) {
			filosofo.start();
		}
	}
	
	/**
	 * Dependiendo de la posición del filósofo, se obtiene un objeto
	 * tenedor de la mesa.
	 *
	 * @param indicePersona El índice del filósofo en la mesa
	 * @return devuelve un objeto tenedor válido
	 */
	private Tenedor obtenerTenedorDerecho(int indicePersona) {
		// El tenedor de la derecha de un filósofo es su misma posición
		// menos una unidad (td = p-1). Pero si la posición es 0
		// entonces se asigna el último tenedor de la mesa (td = tt - 1)
		//
		// Glosario:
		// td = Tenedor de la derecha
		// p = Posición
		// tt = Total tenedores
		int indiceTenedor = indicePersona == 0 ? tenedores.size() - 1 :
							indicePersona - 1;
		return tenedores.get(indiceTenedor);
	}
	
	/**
	 * Dependiendo de la posición del filósofo, se obtiene un objeto
	 * tenedor de la mesa.
	 *
	 * @param indicePersona El índice del filósofo en la mesa
	 * @return devuelve un objeto tenedor válido
	 */
	private Tenedor obtenerTenedorIzquierda(int indicePersona) {
		// Al igual que el tenedor derecho. El tenedor izquierdo es
		// la misma posición del filósofo y en este no es necesario
		// realizar comprobaciones de posición, en caso de desbordamiento.
		return tenedores.get(indicePersona);
	}
	
	/* ---------------------------------------------------------
	 * Extras
	 * --------------------------------------------------------- */
	
	/**
	 * Método principal del programa.
	 *
	 * @param args Argumentos del programa
	 */
	public static void main(String[] args) {
		// Para acceder al teclado podemos utilizar un objeto Scanner,
		// pero este tiende a dar muchos problemas con contenidos en el buffer.
		// Es por eso que en este ejemplo se opta por utilizar la entrada
		// del teclado sin procesar por medio de clases auxiliares como
		// InputStreamReader y BufferedInputStream
		try (var reader = new InputStreamReader(System.in);
			 var bufferedReader = new BufferedReader(reader)
		) {
			// Dado que es un programa que puede cambiar con facilidad
			// le preguntamos al usuario cuantos filósofos quiere que tenga la mesa
			// con este número es posible ver los diferentes resultados y poder
			// jugar con él.
			int numeroPersonas = EntradaUtils.preguntarNumero(
				"Cuantos filósofos se sentarán en la mesa",
				"El contenido no es un numero valido",
				0, 1000, bufferedReader).intValue();
			
			// Creamos una instancia de la clase Comedor
			// que es la encargada de ser el intermediario entre
			// los tenedores y los filósofos
			Comedor comedorFilosofal = new Comedor(numeroPersonas);
			
			// Una vez creado y configurado el comedor, entonces iniciamos los procesos
			// y esperamos a que el programa termine
			comedorFilosofal.iniciarComida();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, null, e);
		}
	}
	
}
