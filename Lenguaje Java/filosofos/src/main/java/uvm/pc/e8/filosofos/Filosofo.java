package uvm.pc.e8.filosofos;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Filosofo extends Thread {
	
	/* ---------------------------------------------------------
	 * Constantes
	 * --------------------------------------------------------- */
	
	/**
	 * Mensaje que muestra que filósofo está pensando
	 */
	private static final String MSG_FILOSOFO_PENSANDO = "El %s esta pensando.\n";
	
	/**
	 * Mensaje que muestra que filósofo está comiendo
	 */
	private static final String MSG_FILOSOFO_COMIENDO = "El %s esta comiendo.\n";
	
	/**
	 * Máximo de tiempo en el que un filósofo puede quedarse pensando.
	 * <p>
	 * El tiempo está representado en milisegundos.
	 */
	private static final long MAX_TIEMPO_PENSAR = 4_000;
	
	/**
	 * Máximo de tiempo en el que un filósofo puede quedarse comiendo.
	 * <p>
	 * El tiempo está representado en milisegundos.
	 */
	private static final long MAX_TIEMPO_COMER = 10_000;
	
	/* ---------------------------------------------------------
	 * Propiedades
	 * --------------------------------------------------------- */
	
	/**
	 * Instancia de registro para esta clase
	 */
	private static final Logger LOG = Logger.getLogger(Filosofo.class.getName());
	
	/**
	 * Instancia de un objeto que genera contenido pseudo-aleatorio.
	 * Esto permite darle más naturalidad al programa.
	 */
	private static final Random RANDOM = new Random(System.nanoTime());
	
	/**
	 * Instancia del comedor donde se van a sentar
	 */
	private final Comedor comedor;
	
	/**
	 * La posición representativa del lugar donde se encuentra en la mesa
	 */
	private final int indice;
	
	/* ---------------------------------------------------------
	 * Constructores
	 * --------------------------------------------------------- */
	
	/**
	 * Constructor por defecto
	 *
	 * @param indice La posición del filósofo en la mesa
	 */
	public Filosofo(Comedor comedor, int indice) {
		// Llamamos el constructor padre
		super();
		// Iniciamos las propiedades
		this.comedor = comedor;
		this.indice = indice;
	}
	
	/* ---------------------------------------------------------
	 * Métodos
	 * --------------------------------------------------------- */
	
	/**
	 * Método ejecutado de forma independiente al hilo principal.
	 * <p>
	 * Este es llamado cuando el método {@link #start()} es llamado
	 * y es cuando el hilo es iniciado dentro de su propio aislado
	 * e interactuando con el programa.
	 */
	@Override
	public void run() {
		// Como el filósofo tiene que verificar, esperar y comer
		// es necesario saber en cualquier momento el estado de los
		// tenedores. Es por eso que se crea un bucle infinito y
		// asi nos aseguramos de conocer el estado de los tenedores
		while (true) {
			// Debido a que el método puede llegar a ser demasiado largo
			// separamos su ejecución en porciones de código más pequeñas.
			// Además, nos aseguramos de verificar si ocurre cualquier error
			try {
				proceso();
				break;
			} catch (Exception e) {
				LOG.log(Level.SEVERE, null, e);
			}
		}
		
		// Cuando el hilo termine, significa que el filósofo pudo comer
		// y se encuentra satisfecho.
		// Es por eso que se muestra únicamente un mensaje de que el
		// filósofo ha terminado
		LOG.info(String.format("El %s esta satisfecho\n", this));
	}
	
	/**
	 * Representación en texto del objeto
	 *
	 * @return representación en texto del objeto
	 */
	@Override
	public String toString() {
		// Hacemos que el índice sea fácil de entender y que estos no
		// empiecen con índices nulos como (0) debido a que puede llegar
		// a confundirse. Esto no modifica el programa, simplemente es
		// utilizado como medio de información al usuario final.
		int indicePersonaHuman = indice + 1;
		return String.format("Filosofo (%d)", indicePersonaHuman);
	}
	
	/**
	 * El proceso que hace el filósofo para poder comer en la mesa
	 *
	 * @throws Exception Error si algo sale mal
	 */
	private void proceso() throws Exception {
		// Debido a que todos los filósofos deben de tener un tiempo para pensar
		// se decidió que la mejor manera de hacerlo es al principio de su proceso.
		// De esta forma nos aseguramos de que todos los filósofos tengan su momento de
		// reflexionar (aunque este paso es completamente opcional).
		ponerseAPensar();
		
		// Después de pensar y meditar las cosas. Es hora de intentar comer.
		// Para esto se hace una petición al proceso padre y le pregunta sobre el estado
		// de los tenedores.
		// Si los tenedores están libres, entonces empieza a comer, de lo contrario, el filósofo
		// se queda en su lugar pensando más tiempo
		ponerseAComer();
		
		// Después de comer, es hora de dejar los tenedores para que otro de sus compañeros
		// pueda tomar asiento y comer de la misma forma que lo hizo él.
		terminarDeComer();
	}
	
	/**
	 * Método que pone al filósofo a pensar mientras espera su turno de comer
	 *
	 * @throws InterruptedException Error al interrumpir el proceso
	 */
	private void ponerseAPensar() throws InterruptedException {
		// El único objetivo de este método es informar al usuario final
		// que el filósofo está pensando y esperando su turno para comer
		LOG.info(String.format(MSG_FILOSOFO_PENSANDO, this));
		
		// Dado que pensar es una acción que debería consumir tiempo, entonces
		// dormimos el hilo un tiempo aleatorio y pasamos a la siguiente acción
		long totalTiempoPensar = RANDOM.nextLong() & MAX_TIEMPO_PENSAR;
		sleep(totalTiempoPensar);
	}
	
	/**
	 * Método que pone al filósofo a comer hasta quedar satisfecho
	 *
	 * @throws InterruptedException Error al interrumpir el proceso
	 */
	private void ponerseAComer() throws InterruptedException {
		// Lo primero que debe hacer el filósofo antes de comer
		// es intentar agarrar los tenedores para comer
		// si estos no están libres entonces se bloquea y espera hasta su turno
		comedor.intentarAgarrarTenedores(indice);
		
		// Después de agarrar los tenedores, es cuando podemos poner un mensaje
		// al usuario que el filósofo actual está comiendo
		LOG.info(String.format(MSG_FILOSOFO_COMIENDO, this));
		
		// Dado que comer es una acción que debería consumir tiempo, entonces
		// dormimos el hilo un tiempo aleatorio y pasamos a la siguiente acción
		long totalTiempoComer = RANDOM.nextLong() & MAX_TIEMPO_COMER;
		sleep(totalTiempoComer);
	}
	
	/**
	 * Método que termina el proceso del filósofo dentro del programa
	 */
	private void terminarDeComer() {
		// Esto podría fácilmente estar dentro del programa sin tener su método
		// propio, pero se hizo para tener una mejor claridad de como funciona el
		// programa y que en un posible futuro, hacerle modificaciones sin el mayor
		// esfuerzo posible.
		// Dado que el filósofo simplemente termina de comer, entonces solo notificamos
		// que los tenedores están libres de nuevo para su uso
		comedor.intentarDejarTenedores(indice);
	}
	
}
