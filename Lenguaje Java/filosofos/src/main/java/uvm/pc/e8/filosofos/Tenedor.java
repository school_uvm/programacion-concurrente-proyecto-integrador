package uvm.pc.e8.filosofos;

/**
 * Clase tenedor. Esta clase por si sola no hace nada
 * y solo representa una entidad dentro del programa.
 * <p>
 * Además, provee información util para sincronizar hilos y
 * este únicamente debe ser controlado por el monitor principal
 * (en este caso la clase {@link Comedor}) debido a que es la
 * que lleva el control de los hilos en ejecución.
 */
public final class Tenedor {
	
	/* ---------------------------------------------------------
	 * Propiedades
	 * --------------------------------------------------------- */
	
	/**
	 * Estado del tenedor que define si el mismo está ocupado o
	 * no. De esta forma él {@link Comedor} es capaz de administrar
	 * y decidir que tenedor puede tomar cada filósofo.
	 */
	private boolean ocupado;
	
	/**
	 * La posición que ocupa el tenedor en la mesa.
	 * <p>
	 * De esta forma es posible en que lugar y que tenedores pueden ser
	 * usados por cualquier filósofo.
	 */
	private final int indice;
	
	/* ---------------------------------------------------------
	 * Constructores
	 * --------------------------------------------------------- */
	
	/**
	 * Constructor por defecto
	 *
	 * @param indice el indice dentro de la mesa
	 */
	public Tenedor(int indice) {
		this.indice = indice;
	}
	
	/* ---------------------------------------------------------
	 * Métodos
	 * --------------------------------------------------------- */
	
	/**
	 * Verifica si el tenedor está ocupado por
	 * alguno de los filósofos.
	 *
	 * @return devuelve el estado del tenedor
	 */
	public boolean estaOcupado() {
		return ocupado;
	}
	
	/**
	 * Cambia el estado del tenedor dependiendo del
	 * parámetro recibido
	 *
	 * @param estado el nuevo estado que se desea asignar
	 */
	public void cambiaOcupado(boolean estado) {
		ocupado = estado;
	}
	
	/**
	 * Representación en texto del objeto
	 *
	 * @return representación en texto del objeto
	 */
	@Override
	public String toString() {
		// Hacemos que el índice sea fácil de entender y que estos no
		// empiecen con índices nulos como (0) debido a que puede llegar
		// a confundirse. Esto no modifica el programa, simplemente es
		// utilizado como medio de información al usuario final.
		int indiceTenedorHuman = indice + 1;
		return String.format("Tenedor (%d)", indiceTenedorHuman);
	}
	
}
