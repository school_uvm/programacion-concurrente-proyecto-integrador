buildscript {
	repositories {
		mavenCentral()
		mavenLocal()
	}
}

allprojects {
	repositories {
		mavenCentral()
		mavenLocal()
	}
}

tasks.create<Delete>("globalClean") {
	allprojects {
		val targetDir = this.file("build")
		delete(targetDir)
	}
}