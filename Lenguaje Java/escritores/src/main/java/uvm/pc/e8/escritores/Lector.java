package uvm.pc.e8.escritores;

import java.util.Random;

public final class Lector extends Thread {
	
	/* ---------------------------------------------------------------
	 * Propiedades
	 * --------------------------------------------------------------- */
	
	/**
	 * Total de tiempo el cual el hilo se va a dormir.
	 * <p>
	 * El tiempo está en milisegundos
	 */
	private static final long MAX_SLEEP_TIME = 500L;
	
	/**
	 * Objeto que genera números pseudoaleatorios
	 */
	private final Random RANDOM = new Random(System.nanoTime());
	
	/**
	 * Objeto que da acceso al objeto padre o proceso principal
	 */
	private final Programa programa;
	
	/* ---------------------------------------------------------------
	 * Constructores
	 * --------------------------------------------------------------- */
	
	/**
	 * Constructor por defecto
	 *
	 * @param programa proceso principal
	 */
	public Lector(Programa programa) {
		super();
		this.programa = programa;
	}
	
	/* ---------------------------------------------------------------
	 * Métodos
	 * --------------------------------------------------------------- */
	
	/**
	 * Proceso de ejecución del hilo
	 */
	@SuppressWarnings("InfiniteLoopStatement")
	@Override
	public void run() {
		// Creamos un bucle infinito
		while (true) {
			programa.inicioLectura();
			
			System.out.println("Leer datos 1");
			sleep();
			System.out.println("Leer datos 2");
			
			programa.finLectura();
			System.out.println("Procesar datos");
			sleep();
		}
	}
	
	/**
	 * Duerme el hilo un cierto límite de tiempo
	 */
	private void sleep() {
		try {
			sleep(RANDOM.nextLong() & MAX_SLEEP_TIME);
		} catch (InterruptedException ignored) {
		}
	}
	
}
