package uvm.pc.e8.escritores;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Programa {
	
	/* ---------------------------------------------------------------
	 * Propiedades
	 * --------------------------------------------------------------- */
	
	/**
	 * Objeto encargado de manejar la sincronización del programa
	 */
	private final ReentrantLock lock = new ReentrantLock();
	
	/**
	 * Condición utilizada para manejar el acceso a los escritores
	 */
	private final Condition escritoresC;
	
	/**
	 * Condición utilizada para manejar el acceso a los lectores
	 */
	private final Condition lectoresC;
	
	/**
	 * El estado actual de los escritores. Utilizado para dar paso
	 * al paso de escritores o lectores.
	 */
	private boolean escritoresStatus;
	
	/**
	 * Contador de lectores. Este contador da acceso a los lectores,
	 * pero no todos a la vez. Esto con el fin de tener un control y que una acción
	 * tenga una prioridad diferente dependiendo de la configuración en el código
	 */
	private int lectoresCount;
	
	/**
	 * Variable encargada de verificar y organizar los elementos para
	 * que los lectores no tomen nuevamente el acceso y no dejen que
	 * los escritores se queden esperando por siempre.
	 */
	private boolean escritorListo;
	
	/* ---------------------------------------------------------------
	 * Constructores
	 * --------------------------------------------------------------- */
	
	/**
	 * Constructor por defecto
	 */
	public Programa() {
		lectoresC = lock.newCondition();
		escritoresC = lock.newCondition();
		lectoresCount = 0;
	}
	
	/* ---------------------------------------------------------------
	 * Métodos
	 * --------------------------------------------------------------- */
	
	/**
	 * Iniciamos la lectura de datos hacemos esperar a los escritores
	 */
	public void inicioLectura() {
		// Bloqueamos los recursos
		lock.lock();
		// Prevenimos cualquier error
		try {
			// Verificamos los recursos y esperamos hasta que alguna de las condiciones
			// termine
			while (escritorListo || escritoresStatus || lock.hasWaiters(escritoresC)) {
				lectoresC.await();
			}
		} catch (Exception ignore) {
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * Finalizamos la lectura y liberamos los recursos
	 */
	public void finLectura() {
		lock.lock();
		try {
			lectoresCount--;
			// Cuando el número de lectores sea igual a cero
			// entonces se liberan los recursos
			if (lectoresCount == 0 && lock.hasWaiters(escritoresC)) {
				escritoresC.signal();
			}
		} catch (Exception ignore) {
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * Iniciamos el proceso de escritura y hacemos esperar a los lectores
	 */
	public void inicioEscritura() {
		lock.lock();
		try {
			while (lectoresCount > 0 || escritoresStatus) {
				escritoresC.await();
				escritorListo = false;
			}
			escritoresStatus = true;
		} catch (Exception ignore) {
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * Finalizamos la escritura y liberamos los recursos
	 */
	public void finEscritura() {
		lock.lock();
		try {
			escritoresStatus = false;
			if (lock.hasWaiters(escritoresC)) {
				escritorListo = true;
				escritoresC.signal();
			} else {
				lectoresC.signalAll();
			}
		} catch (Exception ignore) {
		} finally {
			lock.unlock();
		}
	}
	
	/**
	 * Iniciamos todos los procesos los iniciamos
	 */
	public void ejecutarPrograma() {
		// Creamos los lectores
		for (int i = 0; i < 5; i++) {
			(new Lector(this)).start();
		}
		// Creamos los escritores
		for (int i = 0; i < 3; i++) {
			(new Escritor(this)).start();
		}
	}
	
	/**
	 * Método principal del programa.
	 *
	 * @param args Argumentos del programa
	 */
	public static void main(String[] args) {
		(new Programa()).ejecutarPrograma();
	}
	
}
