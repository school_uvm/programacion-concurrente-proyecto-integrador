plugins {
	java
	application
}

application {
	mainClass.set("uvm.pc.e8.jacobi.Program")
}

java {
	sourceCompatibility = JavaVersion.VERSION_11
	targetCompatibility = JavaVersion.VERSION_11
}

tasks.named<JavaExec>("run") {
	standardInput = System.`in`
	standardOutput = System.`out`
}

dependencies {
	implementation(project(":utilidades"))
}