package uvm.pc.e8.utilidades;

import uvm.pc.e8.utilidades.data.Info;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collection;

public final class EntradaUtils {
	
	/**
	 * Esta clase no puede ser asignada
	 */
	private EntradaUtils() {
	}
	
	/* -----------------------------------------------------
	 * Métodos
	 * ----------------------------------------------------- */
	
	/**
	 * Pregunta al usuario para insertar un texto
	 *
	 * @param pregunta pregunta para el usuario
	 * @param error    error si el contenido está mal
	 * @param reader   entrada del teclado
	 * @return devuelve el texto insertado
	 * @throws IOException error al leer la entrada del texto
	 */
	public static String preguntarTexto(String pregunta, String error, BufferedReader reader) throws IOException {
		String resultado = null;
		boolean valido = false;
		
		// Preguntar por texto
		while (!valido) {
			System.out.printf("%s: ", pregunta);
			resultado = reader.readLine();
			
			// Verificamos si el resultado es válido
			if (resultado == null || resultado.isBlank()) {
				System.out.println(error);
				continue;
			}
			
			// Cambiamos el estado de la variable
			valido = true;
		}
		
		return resultado;
	}
	
	/**
	 * Pregunta al usuario para insertar un numero
	 *
	 * @param pregunta pregunta para el usuario
	 * @param error    error si el contenido está mal
	 * @param reader   entrada del teclado
	 * @return devuelve el numero insertado
	 * @throws IOException error al leer la entrada del texto
	 */
	public static Number preguntarNumero(String pregunta, String error, BufferedReader reader) throws IOException {
		Number resultado = null;
		boolean valido = false;
		
		// Preguntar por número
		while (!valido) {
			String contenido = preguntarTexto(pregunta, "El contenido no debe estar vacío", reader);
			// Convertir texto a numero
			try {
				resultado = Double.parseDouble(contenido);
				valido = true;
			} catch (NumberFormatException e) {
				System.out.println(error);
			}
		}
		
		// Devolvemos el resultado
		return resultado;
	}
	
	/**
	 * Pregunta al usuario para insertar un numero dentro de un rango
	 *
	 * @param pregunta pregunta para el usuario
	 * @param error    error si el contenido está mal
	 * @param min      rango mínimo
	 * @param max      rango máximo
	 * @param reader   entrada del teclado
	 * @return devuelve el numero insertado
	 * @throws IOException error al leer la entrada del texto
	 */
	public static Number preguntarNumero(String pregunta, String error, Number min, Number max, BufferedReader reader) throws
		IOException {
		Number resultado = null;
		boolean valido = false;
		
		while (!valido) {
			resultado = preguntarNumero(pregunta, error, reader);
			
			if (resultado.doubleValue() < min.doubleValue()) {
				System.out.printf("El valor es menor al rango especificado: El valor mínimo es %s\n", min);
				continue;
			}
			
			if (resultado.doubleValue() > max.doubleValue()) {
				System.out.printf("El valor es mayor al rango especificado: El valor máximo es %s\n", max);
				continue;
			}
			
			valido = true;
		}
		
		return resultado;
	}
	
	/**
	 * Realizamos una pregunta para tipos enumerados, algo muy similar a una lista de opciones
	 * pero trabajando directamente con objetos de tipo {@link Enum}
	 *
	 * @param pregunta pregunta para el usuario
	 * @param error    error si el contenido está mal
	 * @param enumType tipo enumerado
	 * @param reader   entrada del teclado
	 * @param <T>      tipo genérico para los objetos {@link Enum}
	 * @return devuelve el tipo enumerado correspondiente a la selección del usuario
	 * @throws Exception error al leer el teclado o el método no se encontró
	 */
	@SuppressWarnings({"unchecked", "ConstantConditions"})
	public static <T extends Enum<T>> T preguntarTipoEnum(String pregunta, String error, Class<T> enumType,
		BufferedReader reader) throws Exception {
		// Obtenemos todos los elementos del tipo enumerado
		Method mValores = enumType.getMethod("values");
		mValores.setAccessible(true);
		// Guardamos los valores del tipo enumerado
		T[] valores = (T[]) mValores.invoke(null);
		T seleccionado = null;
		
		// Si el tipo enumerado esta vació se devuelve nulo
		if (valores.length == 0) return seleccionado;
		
		// Mostramos todas las opciones
		for (int i = 0; i < valores.length; ++i) {
			System.out.printf("(%d) %s\n", i + 1, valores[i]);
		}
		
		// Mostramos la pregunta
		int indice = preguntarNumero(pregunta, error, 1, valores.length, reader).intValue();
		seleccionado = valores[indice - 1];
		
		// Devolvemos el resultado
		return seleccionado;
	}
	
	/**
	 * Preguntamos al usuario por cualquier elemento dentro de la lista pasada como argumento
	 *
	 * @param pregunta   pregunta para el usuario
	 * @param error      error si el contenido está mal
	 * @param collection la colección que se desea mostrar
	 * @param reader     entrada de teclado
	 * @param <T>        tipo genérico para los tipos {@link Collection}
	 * @return devuelve el elemento seleccionado por el usuario
	 * @throws IOException error al leer el teclado
	 */
	public static <T> T preguntarCollection(String pregunta, String error, Collection<T> collection,
		BufferedReader reader) throws IOException {
		// Variables con el elemento final y la cantidad de elementos posibles
		int cantidad = collection.size();
		boolean activo = true;
		T seleccionado = null;
		
		// Mostramos la información del elemento hasta que alguna de las entradas sea válida
		while (activo) {
			// Obtenemos el objeto iterator de la colección
			int contador = 0;
			// Insertamos la información
			for (T elemento : collection) {
				// Revisamos el tipo de objeto
				if (elemento instanceof Info) {
					System.out.printf("%d - %s\n", ++contador, ((Info) elemento).getObjectInfo());
				} else {
					System.out.printf("%d - %s\n", ++contador, elemento);
				}
			}
			
			// Preguntamos por el elemento que se quiere seleccionar
			int indice = preguntarNumero(pregunta, error,
										 1, cantidad, reader).intValue();
			
			// Reiniciamos el contador
			contador = 0;
			// Volvemos a iterar
			for (T elemento : collection) {
				// Solamente verificamos que el elemento sea igual al índice seleccionado
				if (contador == (indice - 1)) {
					seleccionado = elemento;
					activo = false;
					break;
				}
				++contador;
			}
		}
		
		return seleccionado;
	}
	
	/**
	 * Espera a que el usuario presione enter para continuar
	 *
	 * @param reader   entrada del teclado
	 * @param espacios espacios antes de la pausa del programa
	 * @throws IOException error al leer la entrada del teclado
	 */
	public static void esperarEntrada(BufferedReader reader, int espacios) throws IOException {
		String lineas = espacios == 0 ? "" : "\n".repeat(espacios);
		System.out.println(lineas + "Presione enter para continuar...");
		reader.readLine();
	}
	
	/**
	 * Espera a que el usuario presione enter para continuar
	 *
	 * @param reader entrada del teclado
	 * @throws IOException error al leer la entrada del teclado
	 */
	public static void esperarEntrada(BufferedReader reader) throws IOException {
		esperarEntrada(reader, 2);
	}
	
}