package uvm.pc.e8.utilidades.data;

public interface Info {
	
	/**
	 * Devuelve la información del objeto actual
	 *
	 * @return la información del objeto
	 */
	String getObjectInfo();
	
}
