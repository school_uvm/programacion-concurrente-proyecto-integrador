package uvm.pc.e8.utilidades.actions;

import uvm.pc.e8.utilidades.error.InfoException;

import java.util.logging.Level;
import java.util.logging.Logger;

public interface RunnableSafe<T extends Throwable> extends Runnable {
	
	/**
	 * Instancia que genera información .log y verificar errores
	 */
	Logger LOG = Logger.getLogger(RunnableSafe.class.getName());
	
	/**
	 * Ejecución de la interfaz pero con posibilidad de errores
	 *
	 * @throws T Error al ejecutar la acción
	 */
	void runWithErr() throws T;
	
	/**
	 * La ejecución de la interfaz sin errores
	 */
	@Override
	default void run() {
		try {
			runWithErr();
		} catch (Throwable e) {
			if (e instanceof InfoException) {
				LOG.log(Level.SEVERE, e.getMessage());
			} else {
				LOG.log(Level.SEVERE, "Error al ejecutar la acción", e);
			}
		}
	}
	
}
