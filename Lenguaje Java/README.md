# Proyecto integrador Etapa 2 y 3

- Universidad: Universidad Valle de México
- Materia: Programación concurrente
- Participantes:
	- Alvarez Ayala Brian Eduardo
	- Herrera Daniela
	- Medrano Mendez Jaqueline

## Estructura

Este proyecto consta de varios subproyectos. Donde cada uno se encarga
de realizar un ejercicio en específico de los que están definidos dentro
del documento, los cuales los proyectos son los siguientes:

| Nombre     | Comando           | Descripción                                          |
|------------|-------------------|------------------------------------------------------|
| Escritores | ```:escritores``` | Problema de los escritores y lectores                |
| Filósofos  | ```:filosofos```  | Problema de los filósofos con hambre                 |
| Utilidades | ```:utilidades``` | Utilidades utilizadas en los otros ejercicios        |
| Jacobi     | ```:jacobi```     | Método de Jacobi tanto estructurado como concurrente |

Como se puede observar existe algo llamado ```comando``` dentro de la tabla, esto
significa que con base en ese comando vamos a poder [ejecutar](#ejecutar), [compilar](#compilar) y distribuir
ese problema en específico más adelante.

El proyecto ```:utilidades``` no es un proyecto necesario dentro de la actividad, en
él solo se encuentra funcionalidad auxiliar y repetitiva que existe dentro de los demás
ejercicios.

## Requisitos para compilar

Este proyecto no requiere ningún requisito extra parte de tener instalada en la máquina
un compilador de Java en su version ```[JDK | JRE | OpenJDK | Temurin JDK] >= 11```.
Solamente con esto es posible realizar la compilación y la ejecución de los ejercicios.

## Compilar

Dado que este proyecto utiliza ```Gradle``` para compilar, este ya cuenta con una serie de
utilidades para realizar la compilación y ejecución al mismo tiempo.

Lo único que se debe conocer es el comando a ejecutar __(mencionados anteriormente en la tabla)__,
estos normalmente tienen el nombre de la carpeta y debe empezar con ```:``` símbolo.

#### Ejecutar

Para ejecutar el ejercicio una vez conocido su comando, es necesario abrir una terminal o consola
del sistema (Powershell | CMD | Bash) e ir a la carpeta donde se encuentra el proyecto.
Para verificar si estamos en la ruta correcta, podemos ejecutar el siguiente comando:

Linux y Mac

```bash
gradlew :tasks
```

Windows

```bash
gradlew.bat :tasks
```

Si el comando es ejecutado correctamente aparecerá una lista con todos los comandos que puedes
ejecutar. De lo contrario aparecerá un error diciendo que el comando ```gradlew``` no es reconocido.

Ahora que verificamos que se encuentra en el directorio correcto, es posible ejecutar el ejercicio
con el siguiente comando:

Linux y Mac

```bash
gradlew :nombre-proyecto:run
```

Windows

```bash
gradlew.bat :nombre-proyecto:run
```

Donde él ``:nombre-proyecto`` debe remplazarse por cualquiera de los comandos de ejercicios listados
en la tabla de [nombres](#estructura).

#### Crear compilación Jar

Un archivo ```*.jar``` es un archivo de compresión con el mismo mecanismo con el que cuentan los archivos
```*.zip```, con la diferencia de que en él se encuentran todas las clases compiladas de Java y sus activos
como imágenes, archivos que no sean Java, etc.

Es comúnmente utilizado para la distribución de programas escritos en Java, ya que se puede configurar para
que se ejecuten con solo abrir el archivo.

__¿Como generarlos?__

En ```Gradle``` es bastante sencillo, ya que sigue la misma estructura que cuando se [ejecuta](#ejecutar) el programa
la única diferencia entre ejecutar y compilar es el último comando:

Linux y Mac

```bash
gradlew :nombre-proyecto:build
```

Windows

```bash
gradlew.bat :nombre-proyecto:build
```

Después de ejecutar el comando, dentro de la carpeta del ejercicio compilado, aparecerá una nueva carpeta
llamada ```build``` en ella se encontrará todo lo relacionado con la compilación del proyecto.

El archivo ```*jar``` generado se encuentra dentro de build pero en la carpeta ```distributions/nombre-proyecto.zip```.
Dentro de este zip se encuentra el archivo jar generado y su respectivo ejecutable por lotes para cada sistema operativo
(simplemente hay que descomprimirlo y ejecutar).